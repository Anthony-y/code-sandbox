﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace LearnEvents
{
    /// <summary>
    /// This class is what sets off the event.
    /// </summary>
    class Observable
    {
        int num = 1;

        public delegate void DelegateChangedNum(int newNum, int oldNum);
        public event DelegateChangedNum OnIncrementNum;

        public void IncrementNumber()
        {
            int oldNum = num;
            int newNum = num++;

            var handler = OnIncrementNum;

            if (handler != null)
            {
                handler(newNum, oldNum);
            }
        }
    }

    /// <summary>
    /// This class reacts to observer doing something.
    /// It has something called an event handler, which is just a fancy name for a method that does something when an event is fired.
    /// </summary>
    class Observer
    {
        public void IncrementNumberHandler(int newNum, int oldNum)
        {
            Console.WriteLine("Num was changed from " + oldNum + " to " + newNum);
        }
    }

    class Events
    {
        public static void Main(string[] args)
        {
            var observable = new Observable();
            var observer = new Observer();

            var isExit = false;

            observable.OnIncrementNum += observer.IncrementNumberHandler;

            while (!isExit)
            {
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.Escape)
                {
                    isExit = true;
                } else
                {
                    observable.IncrementNumber();
                }
            }

            Console.ReadKey();
        }
    }

}
