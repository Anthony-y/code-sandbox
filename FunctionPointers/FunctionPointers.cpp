#include "stdafx.h"

#include <iostream>

void FunctionPointer(void(*function)(const char*), const char* str);
void sayHi(const char* name);

int main()
{
	FunctionPointer(sayHi, "Anthony");

	return 0;
}

void FunctionPointer(void(*function)(const char*), const char* str) {
	function(str);
}

void sayHi(const char* name) {
	std::cout << "Hi " << name << std::endl;
}