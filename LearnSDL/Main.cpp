// Main.cpp : Defines the entry point for the console application.

#include "stdafx.h"

#include "Player.h"

#include <iostream>
#include <string>

#include <SDL\SDL.h>

SDL_Texture *LoadTexture(std::string filePath, SDL_Renderer *renderTarget);

int main(int argc, char *argv[])
{
	SDL_Window *window = nullptr;
	SDL_Surface *windowSurface = nullptr;

	SDL_Renderer *renderer = nullptr;

	SDL_Rect rect;

	rect.w = 32;
	rect.h = 32;
	rect.x = 150;
	rect.y = 200;
	
	Player player;
	player.setRect(rect);

	const int moveSpeed = 1;

	bool isRunning = true;

	std::string text = "";

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		std::cout << "Failed to initialize SDL." << std::endl;
		return 1;
	}
	
	window = SDL_CreateWindow
	(
		"Learn SDL", 
		SDL_WINDOWPOS_CENTERED, 
		SDL_WINDOWPOS_CENTERED, 
		640, 
		480, 
		SDL_WINDOW_SHOWN
	);

	if (window == nullptr) { 
		std::cout << "Failed to create window." << std::endl;
		return 1;
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

	player.setTexture(LoadTexture("Content/Player.bmp", renderer));

	windowSurface = SDL_GetWindowSurface(window);

	SDL_Event ev;

	const Uint8 *keystate;

	while (isRunning) {

		while (SDL_PollEvent(&ev) != 0) {
			if (ev.type == SDL_QUIT) {
				isRunning = false;
			}
		}

		SDL_RenderClear(renderer);

		player.update();
		player.draw(*renderer);

		SDL_RenderPresent(renderer);
	}
	
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	getchar();

    return 0;
}

SDL_Texture *LoadTexture(std::string filePath, SDL_Renderer *renderTarget) {
	SDL_Texture *texture = nullptr;
	SDL_Surface *surface = SDL_LoadBMP("Content/Player.bmp");

	if (surface == NULL) {
		std::cout << "Surface was nullptr. LoadTexture." << std::endl;
	}

	texture = SDL_CreateTextureFromSurface(renderTarget, surface);

	if (texture == nullptr) {
		std::cout << SDL_GetError() << std::endl;
	}

	// The surface was only used to load a texture in and transfer it to a texture.
	// So I can delete it now :P
	SDL_FreeSurface(surface);
	// Also set it back to 0.
	surface = nullptr;
	
	// Return the texture.
	return texture;

}
