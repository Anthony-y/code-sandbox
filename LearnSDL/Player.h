#pragma once

#include "stdafx.h"

#include <iostream>
#include <string>

#include <SDL\SDL.h>

class Player {
private:
	SDL_Rect rect;
	SDL_Texture *texture;
	int moveSpeed = 1;

	const Uint8 *keystate;

public:
	Player() {}
	~Player() {
		SDL_DestroyTexture(texture);
		delete keystate;
	}

	void setTexture(SDL_Texture *texture) {
		this->texture = texture;
	}

	void setRect(SDL_Rect rect) {
		this->rect = rect;
	}

	void update();
	void draw(SDL_Renderer &renderer);
};