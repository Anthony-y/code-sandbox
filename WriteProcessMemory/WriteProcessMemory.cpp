#include "stdafx.h"

#include <iostream>
#include <Windows.h>

int main(int argc, char *argv[])
{
	int abc = 123;
	int *intptr = &abc;

	std::cout << "ABC" << *intptr << std::endl;
	std::cout << "ABC" << *intptr << std::endl;

	getchar();

	// Find window called ThieveTheCoins, I got this from task manager.
	HWND hWnd = FindWindow(NULL, L"ThieveTheCoins");

	if (!hWnd) {
		std::cout << "Couldn't find window." << std::endl;
	}
	else {
		DWORD procID;

		int value = 240;

		GetWindowThreadProcessId(hWnd, &procID);

		HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, false, procID);

		if (!hProc) {
			std::cout << "Failed to open process." << std::endl;
		}
		else {
			// Some dynamic memory address from an instance of Thieve The Coins I was testing with :P
			int stat = WriteProcessMemory(hProc, (LPVOID)0x0260351C, &value, (DWORD)sizeof(value), NULL);

			if (stat > 0) {
				std::cout << "Memory written to process." << std::endl;
			}
			else {
				std::cout << "Memory couldn't be written to process." << std::endl;
			}

			CloseHandle(hProc);
		}
	}

	getchar();

    return 0;
}

