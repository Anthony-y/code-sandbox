#include "stdafx.h"

#include "Player.h"

void Player::update() {
	keystate = SDL_GetKeyboardState(NULL);

	if (keystate[SDL_SCANCODE_A]) {
		rect.x -= moveSpeed;
		std::cout << "A pressed." << std::endl;
	}
	if (keystate[SDL_SCANCODE_D]) {
		rect.x += moveSpeed;
		std::cout << "D pressed." << std::endl;
	}
	if (keystate[SDL_SCANCODE_W]) {
		rect.y -= moveSpeed;
		std::cout << "W pressed." << std::endl;
	}
	if (keystate[SDL_SCANCODE_S]) {
		rect.y += moveSpeed;
		std::cout << "S pressed." << std::endl;
	}
}

void Player::draw(SDL_Renderer &renderer) {
	SDL_RenderCopy(&renderer, this->texture, NULL, &rect);
}