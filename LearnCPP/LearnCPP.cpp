#include <iostream>
#include <string>

#define DEBUG 1

#if DEBUG
	#define LOG(args) std::cout << args << std::endl;
#else
	#define LOG(x)
#endif

// Declare a simple type.
// This enables me to just type "uint",
// instead of "unsigned int".
typedef unsigned int uint;

// Function prototypes so the compiler knows about the function before we declare them.
// This means that we can call them before we declare them.
void passbyRef(int &x);
void passbyName(int x);
void Greeting(const char* customGreeting);

// Struct test.
// Structs are the same as classes,
// except struct's members are public by default.
struct HelloStruct {
	int age = 10;
	const char *name = "Anthony";

	void SetAge(int newAge) {
		age = newAge;
	}
};

class HelloBot {
private:
	const char *message;
	unsigned int timesToDisplay = 1;
public:
	HelloBot::HelloBot(const char *message, unsigned int timesToDisplay) {
		this->message = message;
		this->timesToDisplay = timesToDisplay;
		Run();
	}
	const char* GetMessage() const { return message; }
	void SetMessage(const char *newMessage) { message = newMessage; }
	unsigned int GetDisplayTime() const { return timesToDisplay; }
	void SetDisplayTimes(unsigned int newTimesToDisplay) { timesToDisplay = newTimesToDisplay; }
	void Run() {
		for (auto i = 0; i < this->timesToDisplay; i++)
		{
			LOG(this->message);
		}
	}
};

class Palindrome {
private:
	std::string text;
	std::string reversedText;
public:
	Palindrome::Palindrome(std::string text) {
		this->text = text;
	}
	void GetInput() { std::getline(std::cin, text); }
	bool Check() {
		reversedText = std::string(text.rbegin(), text.rend());
		if (reversedText == text) {
			return true;
		}
		else {
			return false;
		}
	}
};

class FuncPoint {
public:
	// We pass a pointer to a function called "function",
	// with a const char pointer parameter of the function.
	// const char *customGreeting is the actual parameter.
	void FunctionPointer(void(*function)(const char*), const char *customGreeting) {
		// Call the actual function and pass through the customGreeting.
		function(customGreeting);
	}
};

int main()
{
	// Create an instance pointer of HelloBot called helloBot.
	// The constructor runs the "Run" method so theres no need to run it the first time.
	HelloBot *helloBot = new HelloBot("Hello there!", 0);
	HelloStruct *helloStruct = new HelloStruct;
	Palindrome *palindrome = new Palindrome("racecar");
	FuncPoint *funcPoint = new FuncPoint();

	funcPoint->FunctionPointer(Greeting, "Greetings!");

	LOG(helloStruct->age);
	passbyRef(helloStruct->age);
	LOG(helloStruct->age);

	// But if we change the amount of times to display the message,
	// and the message itself,
	// it obviously won't run again so we have to call "Run" manually.
	helloBot->SetDisplayTimes(15);
	helloBot->SetMessage("Yo!");
	helloBot->Run();

	// Call "pause" on cmd to freeze the program until the user "presses any key".
	system("pause");

	// We have to delete the pointers so that the memory is free.
	delete helloBot;
	delete helloStruct;
	delete palindrome;
	delete funcPoint;

	// Nothing bad happened, so return 0.
    return 0;
}

void passbyRef(int &x)
{
	x = 120;
}

void passbyName(int x) 
{
	x = 11;
}

void Greeting(const char* customGreeting) {
	LOG("Hi from Greeting being called from funcPoint.");
	LOG(customGreeting);
}