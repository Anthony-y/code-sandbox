﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlattenAList
{
    class Flatten
    {
        static void Main(string[] args)
        {
            var list = new List<List<int>>();
            var flattenList = new List<int>();

            var _1 = new List<int>();
            _1.Add(1);
            _1.Add(2);

            var _2 = new List<int>();
            _2.Add(3);
            _2.Add(6);

            list.Add(_1);
            list.Add(_2);

            foreach (List<int> l in list)
            {
                foreach (int i in l)
                {
                    flattenList.Add(i);
                }
            }

            foreach (var i in flattenList)
            {
                Console.WriteLine(i);
            }

            Console.ReadKey();
        }
    }
}
